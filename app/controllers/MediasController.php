<?php

class MediasController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Media::all();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// $input = Input::json();
		
		return Media::create(array(
			'title' 					=> Input::get('title'),	
			'description' 				=> Input::get('description'),
			'url' 						=> Input::get('url'),
			'type' 						=> Input::get('type')
		));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Media::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function uploadimage($id) 
	{
		Log::info('REACHED UPLOAD FUNCTION IN CONTROLLER');

		if (Input::hasFile('image'))
		{
			$file 			= Input::file('image');
			$path 			= Input::file('image')->getRealPath();
			$filename 		= substr( $path, strrpos( $path, '/' ) + 1 );
			$fullpath 		= 'img/' . $filename;

			$media 			= Media::find($id);
			$old_url		= $media->url;
			$media->url 	= $fullpath;
			$media->save();

			Log::info('HERE');
			Log::info($old_url);


			if (File::exists($old_url)) 
			{
				File::delete($old_url);
			}
			// Log::info('$fullpath');
			// Log::info($fullpath);
			// Log::info('$media');
			// Log::info($media);
			// Log::info('$id');
			// Log::info($id);

			$file->move('img/');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$media 					= Media::find($id);
		// $input 					= Input::json();

		$media->title 			= Input::get('title');	
		$media->description 	= Input::get('description');
		$media->type 			= Input::get('type');
		$media->url 			= Input::get('url');

		$media->save();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$media = Media::find($id)->delete();
	}

}