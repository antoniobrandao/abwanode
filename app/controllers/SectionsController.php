<?php

class SectionsController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Section::all();
	}
// 
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// $input = Input::json();
		
		return Section::create(array(
			// content
			'title' 					=> Input::get('title'),	
			'string_id' 				=> Input::get('string_id'),
			'image_id' 					=> Input::get('image_id'),
			// system
			'parent_section_string_id' 	=> Input::get('parent_section_string_id'),
			'page_group' 				=> Input::get('page_group'),
			'position' 					=> Input::get('position')
		));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Section::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// $section 	= Section::find($id);
		// $input 		= Input::json();

		// $section->title 					= $input->title;	
		// $section->string_id 				= $input->string_id;
		// $section->image 					= $input->image;
		// $section->parent_section_string_id 	= $input->parent_section_string_id;
		// $section->page_group				= $input->page_group;
		// $section->position 					= $input->position;

		// $section->save();
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$section 	= Section::find($id);
		$input 		= Input::json();

		$section->title 					= $input->title;	
		$section->string_id 				= $input->string_id;
		$section->image_id 					= $input->image_id;
		$section->parent_section_string_id 	= $input->parent_section_string_id;
		$section->page_group				= $input->page_group;
		$section->position 					= $input->position;

		$section->save();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$section = Section::find($id)->delete();
	}

}