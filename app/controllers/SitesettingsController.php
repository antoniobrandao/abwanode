<?php

class SitesettingsController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return Sitesetting::all();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        // return View::make('sitesettings.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Sitesetting::create(array(
			'title' 					=> Input::get('title'),	
			'subtitle' 					=> Input::get('subtitle'),
			'description' 				=> Input::get('description'),
			'image_id' 					=> Input::get('image_id'),
			'meta_desc' 				=> Input::get('meta_desc'),
			'css_class1' 				=> Input::get('css_class1'),
			'css_class1' 				=> Input::get('css_class1'),
			'css_class1' 				=> Input::get('css_class1'),
			'css_class1' 				=> Input::get('css_class1'),
			'layout' 					=> Input::get('layout'),
			'theme' 					=> Input::get('theme'),
			'mode' 						=> Input::get('mode'),
			'type' 						=> Input::get('type'),
			'google_analytics_id' 		=> Input::get('google_analytics_id'),
			'google_analytics_ev' 		=> Input::get('google_analytics_ev')
		));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Log::info('kjshdfkjhsdkfh skdjfh kjsdh fjksdhfkjsdh f');

        return Sitesetting::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // return View::make('sitesettings.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$site_setting 						= Sitesetting::find($id);
		// $input 								= Input::json();
	
		$site_setting->title 				= Input::get('title');
		$site_setting->subtitle 			= Input::get('subtitle');
		$site_setting->description 			= Input::get('description');
		$site_setting->image_id 			= Input::get('image_id');
		$site_setting->meta_desc			= Input::get('meta_desc');
		$site_setting->css_class1 			= Input::get('css_class1');
		$site_setting->css_class1 			= Input::get('css_class1');
		$site_setting->css_class1 			= Input::get('css_class1');
		$site_setting->css_class1 			= Input::get('css_class1');
		$site_setting->layout 				= Input::get('layout');
		$site_setting->theme 				= Input::get('theme');
		$site_setting->mode 				= Input::get('mode');
		$site_setting->type 				= Input::get('type');
		$site_setting->google_analytics_id 	= Input::get('google_analytics_id');
		$site_setting->google_analytics_ev 	= Input::get('google_analytics_ev');

		$site_setting->save();



		// echo Input::file('image');

		// $file = Input::file('image');
  //   	$file->path = URL::to('public/img/' . Input::file('image')->getClientOriginalName());

    	// Input::file('image')->move('public/img/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// $site_setting = Sitesetting::find($id)->delete();
	}

}
