<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



Route::resource('medias', 		'MediasController');
Route::resource('sections', 	'SectionsController');
Route::resource('sitesettings', 'SitesettingsController');

Route::get('dashboard',
array(
		'before' 	=> 'auth',
		'as'		=> 'dashboard', 
		'uses'		=> 'DashboardController@index'
));

Route::get('/',
array(
		'before' 	=> 'auth',
		'as'		=> 'dashboard', 
		'uses'		=> 'DashboardController@index'
));

Route::post('/uploadimage/{id}',
	array(
		'as'		=> 'uploadimage', 
		'uses'		=> 'MediasController@uploadimage'
));

// Route::post('/imagestore',
// array(
// 		'as'		=>'imageupload', 
// 		'uses'		=>'dashboardcontroller@store'
// ));


// Route::resource('pages', 			'PagesController');

// Route::resource('itemgroups', 		'ItemGroupsController');
// Route::resource('imagegroups', 		'ImageGroupsController');
// Route::resource('articlegroups', 	'ArticleGroupsController');
// Route::resource('filegroups', 		'FileGroupsController');


// AUTH

Route::get('login', function()
{
	return View::make('auth.login');
});

Route::get('signup', function()
{
	return View::make('auth.signup');
});


Route::post('login', function()
{
	$userdata = array(
		'email' => Input::get('email'),
		'password' => Input::get('password')
	);

	if (Auth::check())
	{
	    LOG::warning('BEFORE the user is logged in');
	}
	else
		{
			LOG::warning('BEFORE the user is NOT logged in');
		}

	if (Auth::attempt($userdata, true)) 
	{
		// echo 'successful';

		Auth::login(Auth::user());

		// if (Auth::check())
		// {
		//     LOG::warning('the user is logged in');
		// }

		Session::put('user', Auth::user());

		return Redirect::to('dashboard');
	} 
	else 
	{
		echo "not successful";
		return View::make('auth.login');
	}
});

Route::post('signup', function()
{
	$validated = true;
	Log::info('LOG 1');

	if ($validated) 
	{
		Log::info('LOG 2');
		$userdata = array(
			'username' => Input::get('username'),
			'email' => Input::get('email'),
			'password' => Hash::make(Input::get('password'))
		);	

		$user = new User($userdata);
		$user->save();
		Log::info('LOG 3');
		return Redirect::to('login');
	}
	else
	{
		Log::info('LOG 4');
		echo "bad data";
		return View::make('auth.signup');
	}
});



Route::get('logout', function()
{
	Auth::logout();
	return Redirect::to('login');
});

