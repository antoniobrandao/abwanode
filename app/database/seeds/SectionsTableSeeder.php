<?php

class SectionsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('sections')->truncate();

        $sections  = [

        	[ 
        		'index' => '0', 
        	  	'title' => 'Home',
        	  	'stringurl' => '',
        	  	'type' => 'home',
        	],

        ];

        DB::table('sections')->insert($sections)
    }

}