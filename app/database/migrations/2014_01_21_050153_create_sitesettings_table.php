<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitesettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sitesettings', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('subtitle');
			$table->text('description');
			$table->integer('image_id');
			$table->text('meta_tags');
			$table->text('meta_desc');
			$table->string('css_class1');
			$table->string('css_class2');
			$table->string('css_class3');
			$table->string('css_class4');
			$table->string('layout');
			$table->string('theme');
			$table->string('mode');
			$table->string('type');
			$table->string('google_analytics_id');
			$table->string('google_analytics_ev');
			$table->timestamps();
		});

		DB::table('sitesettings')->insert(array(
			'title' 				=> 'Enter Site Title here',
			'subtitle' 				=> 'Enter Subtitle here (optional)',
			'description' 			=> 'Default description lorem ipsum.',
			'image_id' 				=> '1',
			'meta_desc' 			=> 'Click here to change the meta description',
			'meta_tags' 			=> 'Enter meta tags here, separated by commas',
			'google_analytics_id'	=> 'Click to enter Google Analytics tracking ID'
		));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sitesettings');
	}

}
