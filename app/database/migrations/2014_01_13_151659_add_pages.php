<?php

use Illuminate\Database\Migrations\Migration;

class AddPages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// a default page is added automatically for each section.
		
		// there we can add further pages
		
		// There are no pages free form sections
		
		// one section can contain multiple pages




		// DB::table('pages')->insert(array(
		// 	'title' 		=> 'Homepage',
		// 	'subtitle' 		=> 'Welcome to the website',
		// 	'description' 	=> 'Homepage description',
			
		// 	'string_id' 	=> 'home',
		// 	'pagegroup' 	=> '',

		// 	'articlegroups' => '',
		// 	'mediagroups' 	=> '',
		// 	'itemgroups' 	=> '',
		// 	'filegroups' 	=> '',

		// 	'xtra1' 		=> '',
		// 	'xtra2' 		=> ''
		// ));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Page::truncate();
	}

}