<?php

use Illuminate\Database\Migrations\Migration;

class CreateItemgroups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itemgroups', function($table){
			
			$table->increments('id');
			
			$table->string('string_id');		// to be used in friendly urls (with jquery address)
			$table->string('codename'); 	// group codename PLURAL: Like "Products", "Services"...
			$table->string('categories'); 	// categories present in group

			$table->string('title');
			$table->string('subtitle');
			$table->text('description');

			$table->text('elements');		// Array in string format of the ids of contained elements (items in this case)
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itemgroups');
	}

}