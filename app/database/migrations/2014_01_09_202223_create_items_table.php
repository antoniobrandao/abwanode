<?php

use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function($table){
			
			$table->increments('id');
			
			$table->string('codename'); 		// SINGULAR: Like "Product", "Service", etc
			$table->string('category'); 		// category

			$table->string('title');
			$table->string('subtitle');
			$table->text('description');
			$table->text('content_template');	// array of IDs of element groups

			// associated content groups
			$table->text('articles'); 			// array of IDs of single group in string format
			$table->text('medias');				// array of IDs of single group in string format
			$table->text('files');				// array of IDs of single group in string format

			$table->string('xtra1');			// any string value
			$table->string('xtra2');			// any string value

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}