<?php

use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function($table){
			
			$table->increments('id');
			
			$table->string('title');
			$table->string('subtitle');
			$table->text('description');
			$table->string('image');

			$table->string('string_id');		// to be used in friendly urls (with jquery address)
			
			$table->string('analytics_id');		// to be used in inner analytics events

			$table->string('page_group');		// ID of the page group this page belong to

			$table->text('article_groups'); 	// array of IDs in string format 	// can contain multiple article groups
			$table->text('media_groups');		// array of IDs in string format 	// can contain multiple media groups
			$table->text('file_groups');		// array of IDs in string format 	// can contain multiple file groups
			
			$table->text('item_groups');		// array of IDs in string format 	// can contain multiple item groups

			$table->string('xtra1');		// any string value
			$table->string('xtra2');		// any string value
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}