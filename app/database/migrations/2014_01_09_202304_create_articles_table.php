<?php

use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles', function($table){
			
			$table->increments('id');
			
			// 4 parallel indexes
			$table->text('textblocks_text'); 
			$table->text('textblocks_html_tags');
			$table->text('textblocks_classes');
			
			$table->string('vertical_padding_indexes');
			
			$table->text('category');
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articles');
	}

}