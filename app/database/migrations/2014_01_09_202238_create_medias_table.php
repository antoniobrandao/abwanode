<?php

use Illuminate\Database\Migrations\Migration;

class CreateMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medias', function($table){
			
			$table->increments('id');
			
			$table->string('title');
			$table->text('description');

			$table->string('type');
			$table->string('url');
		});

		DB::table('medias')->insert(array(
			'title' 		=> 'Media title',
			'description' 	=> 'Media Subtitle',
			'type' 			=> 'image',
			'url' 			=> 'http://lorempixel.com/400/200/'
		));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medias');
	}

}