<?php

use Illuminate\Database\Migrations\Migration;

class CreateFilegroups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('filegroups', function($table){
			
			$table->increments('id');
			
			$table->string('title');
			$table->string('subtitle');
			$table->text('description');

			$table->text('elements');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('filegroups');
	}

}