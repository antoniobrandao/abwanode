<?php

class Page extends Eloquent {
	public $table = 'pages';
	public $timestamps = false;

	protected $fillable = array(
		'title', 
		'subtitle', 
		'description', 
		'image_url', 
		'string_id', 
		'page_group', 
		'article_groups', 
		'media_groups', 
		'file_groups', 
		'item_groups', 
		'xtra1', 
		'xtra2'
	);

	public static function validate($data) 
	{
		$rules = array(
			'title' => 'required|min:1'
		);

		return Validator::make($data, $rules);
	}
}