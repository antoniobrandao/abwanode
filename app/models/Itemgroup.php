<?php

class Articlegroup extends Eloquent {
	public $table = 'articlegroups';
	public $timestamps = false;

	protected $fillable = array('string_id', 'codename', 'categories', 'title', 'subtitle', 'description', 'elements');

	public static function validate($data) 
	{
		$rules = array(
			'title' => 'required|min:1'
		);

		return Validator::make($data, $rules);
	}
}