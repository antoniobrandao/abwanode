<?php

class Section extends Eloquent {
	public $table = 'sections';
	public $timestamps = false;

	protected $fillable = array(
		'title',
		'string_id',
		'image_id',
		'page_group',
		'parent_section_string_id',
		'position'
	);

	public static function validate($data) 
	{
		$rules = array(
			'title' => 'required|min:2'
		);

		return Validator::make($data, $rules);
	}
}