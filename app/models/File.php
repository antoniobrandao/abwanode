<?php

class Media extends Eloquent {
	public $table = 'medias';
	public $timestamps = false;

	protected $fillable = array('title', 'subtitle', 'description', 'type', 'url', 'size');

	public static function validate($data) 
	{
		$rules = array(
			'title' => 'required|min:1'
		);

		return Validator::make($data, $rules);
	}
}