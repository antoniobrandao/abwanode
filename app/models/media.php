<?php

class Media extends Eloquent {
	public $table = 'medias';
	public $timestamps = false;

	protected $fillable = array(
		'title', 
		'description', 
		'type', 
		'url'
	);

	public static function validate($data) 
	{
		$rules = array(
			'url' => 'required|min:1'
		);

		return Validator::make($data, $rules);
	}
}