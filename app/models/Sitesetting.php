<?php

class Sitesetting extends Eloquent {

	public 			$table 		= 'sitesettings';
	// protected 		$guarded 	= array();
	public 			$timestamps = false;
	// public static 	$rules 		= array();

	protected $fillable = array(
		'title', 
		'subtitle', 
		'description', 
		'image_id', 
		'meta_desc', 
		'meta_tags', 
		'css_class_1', 
		'css_class_2', 
		'css_class_3', 
		'css_class_4', 
		'layout', 
		'mode', 
		'type', 
		'google_analytics_id',
		'google_analytics_ev'
	);

	public static function validate($data) 
	{
		$rules = array(
			'title' => 'required|min:1'
		);

		return Validator::make($data, $rules);
	}
}