<?php

class Filegroup extends Eloquent {
	public $table = 'filegroups';
	public $timestamps = false;

	protected $fillable = array('title', 'subtitle', 'description', 'elements');

	public static function validate($data) 
	{
		$rules = array(
			'title' => 'required|min:1'
		);

		return Validator::make($data, $rules);
	}
}