<!DOCTYPE html>
<html>
	
	<head>
		@if(isset($title))
			<title>{{$title}}</title>
		@else
			<title>Hardcoded Title !!</title>
		@endif

		{{ HTML::style('css/backendstyles.css'); }}
	</head>

	<body style="padding:0;margin:0;">
		
		<div class="header" style="padding:5px;display:block;width:100%;height:20px;background-color:#333; color:#fff">
			<div style="display:inline-block">
				<p id="header-title" style="display:inline">{{$title}}</p>
			</div>
			<div style="float:right;display:inline-block;margin-right:20px">
				<p style="display:inline-block;margin:0;"> . </p>
				<p style="display:inline-block;margin:0;">Antonio Brandao</p>
			</div>
		</div>

	 	{{ HTML::script('js/libs/underscore.js') }}
	 	{{ HTML::script('js/libs/jquery.js') }}
	 	{{ HTML::script('js/libs/backbone.js') }}
	 	{{ HTML::script('js/libs/handlebars.js') }}
	 	{{ HTML::script('js/libs/backbone_handlebars.js') }}

	 	{{ HTML::script('js/backend/backbone_elements.js') }}
	 	{{ HTML::script('js/backend/backbone_application.js') }}

	 	{{ HTML::script('js/plugins/device.min.js') }}
	 	<!-- {{ HTML::script('js/plugins/flowtype.js') }} -->
	 	<!-- {{ HTML::script('js/plugins/jquery.mmenu.min.all.js') }} -->
	 	{{ HTML::script('js/plugins/offline.min.js') }}
	 	{{ HTML::script('js/plugins/pace.min.js') }}
	 	{{ HTML::style('css/plugins/pace.theme.css') }}

		<div class="maincontainer" style="padding:10px;">
			
			@if(Session::has('message'))
				<p style="color:green;"> {{ Session::get('message') }} </p>
			@endif

			
		</div>
		
			@yield('content')
		
	</body>

</html>