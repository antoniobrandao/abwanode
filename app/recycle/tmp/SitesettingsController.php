<?php

class SitesettingsController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Sitesetting::all();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// $input = Input::json();
		
		return Sitesetting::create(array(
			// content
			'title' 					=> Input::get('title'),	
			'subtitle' 					=> Input::get('subtitle'),
			'description' 				=> Input::get('description'),
			'image_url' 				=> Input::get('image_url'),
			'meta_desc' 				=> Input::get('meta_desc'),
			'css_class1' 				=> Input::get('css_class1'),
			'css_class1' 				=> Input::get('css_class1'),
			'css_class1' 				=> Input::get('css_class1'),
			'css_class1' 				=> Input::get('css_class1'),
			'layout' 					=> Input::get('layout'),
			'theme' 					=> Input::get('theme'),
			'mode' 						=> Input::get('mode'),
			'type' 						=> Input::get('type'),
			'google_analytics_id' 		=> Input::get('google_analytics_id'),
			'google_analytics_ev' 		=> Input::get('google_analytics_ev')
		));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Sitesetting::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$site_setting 						= Sitesetting::find($id);
		$input 								= Input::json();
	
		$site_setting->title 				= $input->title	
		$site_setting->subtitle 			= $input->subtitle;
		$site_setting->description 			= $input->description;
		$site_setting->image_url 			= $input->image_url;
		$site_setting->meta_desc			= $input->meta_desc;
		$site_setting->css_class1 			= $input->css_class1;
		$site_setting->css_class1 			= $input->css_class1;
		$site_setting->css_class1 			= $input->css_class1;
		$site_setting->css_class1 			= $input->css_class1;
		$site_setting->layout 				= $input->layout;
		$site_setting->theme 				= $input->theme;
		$site_setting->mode 				= $input->mode;
		$site_setting->type 				= $input->type;
		$site_setting->google_analytics_id 	= $input->google_analytics_id;
		$site_setting->google_analytics_ev 	= $input->google_analytics_ev;

		$site_setting->save();
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$site_setting 						= Sitesetting::find($id);
		$input 								= Input::json();
	
		$site_setting->title 				= $input->title	
		$site_setting->subtitle 			= $input->subtitle;
		$site_setting->description 			= $input->description;
		$site_setting->image_url 			= $input->image_url;
		$site_setting->meta_desc			= $input->meta_desc;
		$site_setting->css_class1 			= $input->css_class1;
		$site_setting->css_class1 			= $input->css_class1;
		$site_setting->css_class1 			= $input->css_class1;
		$site_setting->css_class1 			= $input->css_class1;
		$site_setting->layout 				= $input->layout;
		$site_setting->theme 				= $input->theme;
		$site_setting->mode 				= $input->mode;
		$site_setting->type 				= $input->type;
		$site_setting->google_analytics_id 	= $input->google_analytics_id;
		$site_setting->google_analytics_ev 	= $input->google_analytics_ev;

		$site_setting->save();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$site_setting = Sitesetting::find($id)->delete();
	}

}