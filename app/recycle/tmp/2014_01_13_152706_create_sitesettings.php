<?php

use Illuminate\Database\Migrations\Migration;

class CreateSitesettings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('site_settings', function($table){
			
			$table->increments('id');
			
			$table->string('title');
			$table->string('subtitle');
			$table->text('description');

			$table->text('image_url');
			$table->text('meta_desc');
			$table->text('meta_tags');

			$table->string('css_class_1');
			$table->string('css_class_2');
			$table->string('css_class_3');
			$table->string('css_class_4');

			$table->string('layout');
			$table->string('theme');
			$table->string('mode');
			$table->string('type');

			$table->string('google_analytics_id');
			$table->string('google_analytics_ev');
		});

		DB::table('site_settings')->insert(array(
			'title' 				=> 'Enter Site Title here',
			'subtitle' 				=> 'Enter Subtitle here (optional)',
			'description' 			=> 'Default description lorem ipsum.',
			'image_url' 			=> 'http://lorempixel.com/400/200/',
			'meta_desc' 			=> 'Click here to change the meta description',
			'meta_tags' 			=> 'Enter meta tags here, separated by commas',
			'google_analytics_id'	=> 'Click to enter Google Analytics tracking ID'
		));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('site_settings');
	}

}





