<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
	<style>
		table thead td { font-weight: bold; }
		#allContacts { margin-top: 2em; margin-bottom: 1em; }
	</style>
</head>
<body>
	<div class="welcome">
		<h1>Front-End</h1>
	</div>

	<form id="addContact">
		<div>
			<label for="first_name">First Name: </label>
			<input type="text" id="first_name" name="first_name">
		</div>
		<div>
			<label for="last_name">Last Name: </label>
			<input type="text" id="last_name" name="last_name">
		</div>
		<div>
			<label for="email_address">Email address: </label>
			<input type="text" id="email_address" name="email_address">
		</div>
		<div>
			<label for="description">Description: </label>
			<textarea id="description" name="description"></textarea>
		</div>
		<input type="submit" value="Submit">

	</form>

	<table id="allContacts">
		<thead>
			<tr>
				<td>First Name</td>
				<td>Last Name</td>
				<td>Email Address</td>
				<td>Description</td>
			</tr>
		</thead>
	</table>

	<div id="editContact">
		
	</div>

	<script id="allContactsTemplate" type="text/template">
		<span><%= first_name %></span>
		<span><%= last_name %></span>
		<span><%= email_address %></span>
		<span><%= description %></span>
		<a href="#contacts/<%= id %>/edit" class="edit">Edit</a>
		<a href="#contacts/<%= id %>" class="delete">Delete</a>
	</script>

	<script id="editContactTemplate" type="text/template">
		<form id="editContact">
			<div>
				<label for="edit_first_name">First Name: </label>
				<input type="text" id="edit_first_name" name="edit_first_name" value="<=% first_name %>">
			</div>
			<div>
				<label for="edit_last_name">Last Name: </label>
				<input type="text" id="edit_last_name" name="edit_last_name" value="<=% last_name %>">
			</div>
			<div>
				<label for="edit_email_address">Email address: </label>
				<input type="text" id="edit_email_address" name="edit_email_address" value="<=% email_address %>">
			</div>
			<div>
				<label for="edit_description">Description: </label>
				<textarea id="edit_description" name="edit_description"><=% description %></textarea>
			</div>
			<input type="submit" value="Update Contact">

		</form>
	</script>

	<!-- {{ Contact::all() }} -->

 	{{ HTML::script('js/libs/underscore.min.js') }}
 	{{ HTML::script('js/libs/jquery.min.js') }}
 	{{ HTML::script('js/libs/backbone.js') }}

 	{{ HTML::script('js/backend/main.js') }}
 	{{ HTML::script('js/backend/models.js') }}
 	{{ HTML::script('js/backend/collections.js') }}
 	{{ HTML::script('js/backend/views.js') }}
 	{{ HTML::script('js/backend/router.js') }}

 	<!-- {{ HTML::script('js/plugins/device.min.js') }} -->
 	<!-- {{ HTML::script('js/plugins/flowtype.js') }} -->
 	<!-- {{ HTML::script('js/plugins/jquery.mmenu.min.all.js') }} -->
 	<!-- {{ HTML::script('js/plugins/offline.min.js') }} -->
 	<!-- {{ HTML::script('js/plugins/pace.min.js') }} -->

 	<script>

	 	new App.Router;

	 	Backbone.history.start();

	 	App.contacts = new App.Collections.Contacts;

	 	App.contacts.fetch().then(function(){
	 		new App.Views.App({ collection: App.contacts})
	 	})

 	</script>

</body>
</html>
