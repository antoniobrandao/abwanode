@if(isset($errors))
	<ul>
		{{ $errors->first('position', '<li>:message</li>') }}
		{{ $errors->first('title', '<li>:message</li>') }}
	</ul>
@endif