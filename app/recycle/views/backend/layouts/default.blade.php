<!DOCTYPE html>
<html>
	
	<head>
		@if(isset($title))
			<title>{{$title}}</title>
		@else
			<title>Hardcoded Title !!</title>
		@endif

		{{ HTML::style('css/backendstyles.css'); }}
	</head>


	 	{{ HTML::script('js/libs/underscore.min.js') }}
	 	{{ HTML::script('js/libs/jquery.min.js') }}
	 	{{ HTML::script('js/libs/backbone.min.js') }}
	 	<!-- {{ HTML::script('js/plugins/device.min.js') }} -->
	 	<!-- {{ HTML::script('js/plugins/flowtype.js') }} -->
	 	<!-- {{ HTML::script('js/plugins/jquery.mmenu.min.all.js') }} -->
	 	<!-- {{ HTML::script('js/plugins/offline.min.js') }} -->
	 	<!-- {{ HTML::script('js/plugins/pace.min.js') }} -->
	 	{{ HTML::script('js/backend/main.js') }}
	 	{{ HTML::script('js/backend/models.js') }}
	 	{{ HTML::script('js/backend/collections.js') }}
	 	{{ HTML::script('js/backend/views.js') }}
	 	{{ HTML::script('js/backend/router.js') }}
		
	</body>
	

</html>