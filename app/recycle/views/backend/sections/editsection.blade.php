@extends('backend.layouts.default')

@section('content')
	<h1>Editing {{ $section->title }} </h1>

	{{ View::make('backend.common.section_errors' ) }}

	{{ Form::open(array('route' => 'update_section', 'method' => 'put')) }}
	
	{{ Form::token() }}

	<p>
		{{ Form::label('title', 'Title:') }} <br />
		{{ Form::text('title', $section->title) }} 
	</p>

	<p>
		{{ Form::label('stringurl', 'Stringurl:') }} <br />
		{{ Form::text('stringurl', $section->stringurl) }} 
	</p>

	<p>
		{{ Form::label('type', 'Type:') }} <br />
		{{ Form::text('type', $section->type) }} 
	</p>

	<p>
		{{ Form::label('position', 'Position:') }} <br />
		{{ Form::text('position', $section->position) }} 
	</p>

	{{ Form::hidden('id', $section->id) }}

	<p>{{ Form::submit('Update Section')}}</p>

	{{ Form::close() }}
@stop 