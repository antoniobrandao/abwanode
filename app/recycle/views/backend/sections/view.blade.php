@extends('backend.layouts.default')

@section('content')
	<h1> {{ e($section->title) }}</h1>
	<p>Index = {{ e($section->position) }}</p>

	<span>
		{{ HTML::linkRoute('edit_section', 'Edit', array($section->id)) }} <br />
		{{ HTML::linkRoute('dashboard', 'Back to Admin') }}
		
		<br />
		<br />

		{{ Form::open(array('route' => 'delete_section', 'method' => 'delete')) }}
		{{ Form::token() }}
		{{ Form::hidden('id', $section->id) }}
		{{ Form::submit('Delete') }}
		{{ Form::close() }}
	</span>
@stop