@extends('backend.layouts.default')

@section('content')
	@if(isset($title))
		<h1>{{ $title }}</h1>
		title found
	@else
		<h1>Sections Home page</h1>
		No var found
	@endif

	<ul>

		@foreach($sections as $section)
			<li>  {{ HTML::linkRoute('section', $section->title, array($section->id)) }} </li>
		@endforeach
		
	</ul>

	<p> {{ HTML::linkRoute('new_section', 'New Section') }} </p>
@stop 