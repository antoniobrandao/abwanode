@extends('backend.layouts.default')

@section('content')
	<h1>Add New Section</h1>

	{{ View::make('backend.common.section_errors' )}}

	{{ Form::open(array('route' => 'create_section')) }}
	
	{{ Form::token() }}

	<p> 
		{{ Form::label('title', 'Title:') }} <br />
		{{ Form::text('title', Input::old('title')) }} 
	</p>
	<p> 
		{{ Form::label('stringurl', 'String URL:') }} <br />
		{{ Form::text('stringurl', Input::old('stringurl')) }} 
	</p>
	<p> 
		{{ Form::label('type', 'Type:') }} </br />
		{{ Form::text('type', Input::old('type')) }} 
	</p>
	<p> 
		{{ Form::label('position', 'Position:') }} </br />
		{{ Form::text('position', Input::old('position')) }} 
	</p>
	
	{{ Form::submit('Add Section') }} 

	{{ Form::close() }} 
@stop