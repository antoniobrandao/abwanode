<?php

class TasksController extends BaseController {

	public $restful = true;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Task::all();

		// return View::make('frontend.hello');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Task::create(array(
			'title' 	=> Input::get('title'),
			'completed' => Input::get('completed')
		));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Task::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// $section = Section::find($id);

		// $section->title 	= Input::get('title');
		// $section->stringid  = Input::get('stringid');
		// $section->type 		= Input::get('type');
		// $section->position 	= Input::get('position');

		// $section->save();



		// return Task::find($id);
	
		// return View::make('frontend.hello');

		// return Task::all();

		// return Input::get('title');

		$task = Task::find($id);

		$task->title = Input::get('title');
		$task->completed = Input::get('completed');

		$task->save(); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$task = Task::find($id)->delete();
	}

}
