/// ///  ROUTER  /// ///
    /// ///  ROUTER  /// ///
    /// ///  ROUTER  /// ///


    App.Router = Backbone.Router.extend({
        routes: {
            '': 'index'
            // 'show/:id': 'show',
            // 'download/:id/*filename': 'download',
            // 'search/:query': 'search',
            // 'appointment/:id': 'showAppointment',
            // '*other': 'default'
        },

        index: function() {
            console.log('index route reached');
        }

        // showAppointment: function(appointmentId) {
        //     console.log('show appointment with id = ' + appointmentId);
        //     vent.trigger('appointment:show', appointmentId);
        // },

        // show: function(id) {
        //     console.log('show item with id = ' + id);
        // },

        // download: function(id, filename) {
        //     console.log(filename);
        // },

        // search: function(query) {
        //     console.log(query);
        // },

        // default: function(other) {
        //     console.log(other + ' not found 404'); // 404
        // }
    });

    // new App.Router;

    // Backbone.history.start();