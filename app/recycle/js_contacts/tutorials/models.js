App.Models.Contact = Backbone.Model.extend({
	
	idAttribute  : "id",

	initialize: function(){
		console.log('App.Models.Contact ::: initialize');
		this.bind("change", this.attributesChanged);
	},

	attributesChanged: function(){
		console.log('App.Models.Contact ::: attributesChanged');
		// var valid = false;
		// if (this.get('username') && this.get('password'))
		//   valid = true;
		// this.trigger("validated", valid);
	},

	validate: function(attrs) {
		if ( !attrs.email_address ) {
			return "email_address are required";
		};
	},

	defaults: {
        'first_name':'John',
        'last_name':'Doe'
    }

    // urlRoot: '/contacts'
}); 
