 (function(){

    // _.templateSettings = {
    //     interpolate : /\{\{([\s\S]+?)\}\}/g
    // };

    window.App = 
    {
        Models: {},
        Collections: {},
        Views: {}
        // Router: {}
    };

    var vent = _.extend({}, Backbone.Events);

    $(document).ajaxError(function (e, xhr, options) 
    {    
        console.log('- AJAX log -');
        // console.log('e: ' + e);
        console.log('xhr: ' + xhr.responseText);
        // console.log('options: ' + options.responseText);
    });

    /// ///  APP  /// ///
    /// ///  APP  /// ///
    /// ///  APP  /// ///

    // window.App = 
    // {
    //     Models: {},
    //     Collections: {},
    //     Views: {},
    //     Router: {}
    // };


    /// ///  HELPERS  /// ///
    /// ///  HELPERS  /// ///
    /// ///  HELPERS  /// ///

    window.template = function(id) {
        return _.template( $('#' + id).html() );
    };
    
    // window.assign = function (view, selector) {
    //     view.setElement(this.$(selector)).render();
    // };

    // to use assign:
    // render : function () {
    //     this.$el.html(this.template());

    //     this.assign(this.subview,        '.subview');
    //     this.assign(this.anotherSubview, '.another-subview');
    //     return this;
    // }

    /// ///  EVENTS  /// ///
    /// ///  EVENTS  /// ///
    /// ///  EVENTS  /// ///


    // var vent = _.extend({}, Backbone.Events);


    /// ///  ROUTER  /// ///
    /// ///  ROUTER  /// ///
    /// ///  ROUTER  /// ///


    // App.Router = Backbone.Router.extend({
    //     routes: {
    //         '': 'index'
            // 'show/:id': 'show',
            // 'download/:id/*filename': 'download',
            // 'search/:query': 'search',
            // 'appointment/:id': 'showAppointment',
            // '*other': 'default'
        // },

        // index: function() {
        //     console.log('index route reached');
        // }

        // showAppointment: function(appointmentId) {
        //     console.log('show appointment with id = ' + appointmentId);
        //     vent.trigger('appointment:show', appointmentId);
        // },

        // show: function(id) {
        //     console.log('show item with id = ' + id);
        // },

        // download: function(id, filename) {
        //     console.log(filename);
        // },

        // search: function(query) {
        //     console.log(query);
        // },

        // default: function(other) {
        //     console.log(other + ' not found 404'); // 404
        // }
    // })

    // new App.Router;

    // Backbone.history.start();


    /// /// APP SPECIFIC /// ///
    /// /// APP SPECIFIC /// ///
    /// /// APP SPECIFIC /// ///


    // App.Models.Task = Backbone.Model.extend({
    //     defaults: {
    //         title: 'lalala',
    //         completed: 0
    //     }
    // })

    // App.Collections.Tasks = Backbone.Collection.extend({
    //     model: App.Models.Task,
    //     url: '/tasks'
    // });

    // App.Views.Tasks = Backbone.View.extend({
    //     tagName: 'ul',

    //     initialize: function() {
    //         this.collection.on('add', this.addOne, this);
    //     },

    //     render: function() {
    //         this.$el.empty();
    //         this.collection.each(this.addOne, this);
    //         return this;
    //     },

    //     addOne: function(task) {
    //         var task = new App.Views.Task({ model: task });
    //         this.$el.append( task.render().el );
    //     }
    // });

    // App.Views.Task = Backbone.View.extend({
    //     tagName: 'li',

    //     initialize: function() {
    //         this.model.on('destroy', this.remove, this)
    //     },

    //     render: function( ) {
    //         this.$el.html( this.model.get('title') );
    //         return this;
    //     }
    // });

}());



    