

/*
|---------------------------------------------
|
| Global App view
|
|---------------------------------------------
*/

		var vent = _.extend({}, Backbone.Events);

App.Views.App = Backbone.View.extend({

	initialize: function(){


		vent.on('contact:edit', this.editContact, this);

		console.log('----- ----- -----                                  ----- ----- ----- ');
		console.log('----- ----- -----   App.Views.App ::: initialize   ----- ----- ----- ');
		console.log('----- ----- -----                                  ----- ----- ----- ');
		
		console.log( this.collection.toJSON() );
		
		var allContactsView = new App.Views.Contacts({ collection: App.contacts }).render();
		
		var addContactView = new App.Views.AddContact({ collection: App.contacts });
		
		$(document.body).append(allContactsView.el);

		console.log('----- ----- -----                                  ----- ----- ----- ');
		console.log('----- ----- ----- App.Views.App ::: initialize end ----- ----- ----- ');
		console.log('----- ----- -----                                  ----- ----- ----- ');
	},

	editContact: function(contact) {
		console.log('contact: ' + contact);
		var editContactView = new App.Views.EditContact({ model: contact });
		console.log('editContactView.el = ' + editContactView.el);
		$('#editContact').html(editContactView.el);
	}
});


/*
|---------------------------------------------
|
| Single contact view
|
|---------------------------------------------
*/

App.Views.Contact = Backbone.View.extend({
	tagName: 'span',

	template: template('allContactsTemplate'),

	initialize: function(){
		console.log('App.Views.Contact ::: initialize');

		this.model.on('destroy', this.unrender, this);

		// this.model.on('sync', this.onAAyncEvent, this);
		// this.model.on('reset', this.onTestEvent, this);

	},

	events: {
		'click a.delete': 'deleteContact',
		'click a.edit': 'editContact'
	},

	// testEvent: function() {
	// 	console.log("0988098098098");
	// },

	deleteContact: function() {

		console.log('App.Views.Contact ::: deleteContact');

		this.model.destroy();
	},

	editContact: function() {
		vent.trigger('contact:edit', this.model);
		// console.log('App.Views.Contact ::: editContact');
		// console.log('App.Views.Contact ::: editContact - this.model: ' + this.model);

		// this.model.fetch( {success : console.log('333 33 3 3 3 33 3 this.model: '+ this.model) });

		// create new EditContactView
		// bind the model
		// append to the DOM

		
	},

	// onAAyncEvent: function(model) {
	// 	console.log('SYNC: detected sync');

		// var editContactView = new App.Views.EditContact({ model: this.model });
	// },

	render: function() {

		console.log('App.Views.Contact ::: render');

		this.$el.html( this.template( this.model.toJSON() ) );
		return this;
	},

	unrender: function() {

		console.log('App.Views.Contact ::: unrender');

		this.remove();  // this.$el.remove
	}
});


/*
|---------------------------------------------
|
| All contacts view
|
|---------------------------------------------
*/


App.Views.Contacts = Backbone.View.extend({
	
	tagName: 'div',

	initialize: function() {

		console.log('App.Views.Contacts ::: initialize');

		this.collection.on('change',  this.onChangeEvent );

		this.collection.on('add', this.addOne, this);
		// this.collection.on('reset', this.onResetEvent, this);
		// this.collection.on('sync', this.onAAyncEvent, this);
		
		// this.collection.on('reset', function(model) {
		// 	console.log('RESET: detected fetch');
		// });

		// this.collection.on('sync', function(model) {
		// 	console.log('SYNC: detected fetch');
		// });
	},
	
	onChangeEvent: function(model) {

		console.log('App.Views.Contacts ::: onChangeEvent');
		App.contacts.fetch();
	},

	// onResetEvent: function(model) {

	// 	console.log('App.Views.Contacts ::: onResetEvent');
	// },

	// onAAyncEvent: function(model) {
	// 	console.log('SYNC: detected sync');
	// },

	render: function() {

		console.log('App.Views.Contacts ::: render ' + 'this.el: ' + this.el);

		this.$el.empty();

		this.collection.each( this.addOne, this );
		return this;
	},

	addOne: function(contactModel) {

		console.log('App.Views.Contacts ::: addOne');


		var contactView = new App.Views.Contact({ model: contactModel });
		// contactModel.fetch();
		this.$el.append(contactView.render().el);
		this.$el.append('<br />');
		this.$el.append('<br />');

		// console.log('contactView.id: '+ contactView.id);

	}
});


/*
|---------------------------------------------
|
| Add contact view
|
|---------------------------------------------
*/


App.Views.AddContact = Backbone.View.extend({
	el: '#addContact',

	initialize: function() {

		console.log('App.Views.AddContact ::: initialize');

		//caching
		this.first_name 	= $('#first_name');
		this.last_name 		= $('#last_name');
		this.email_address 	= $('#email_address');
		this.description 	= $('#description');
	},

	events: {
		'submit': 'addContact'
		// 'sync': 'onSync'
	},

	// onSync: function(e) {

	// 	console.log('App.Views.AddContact ::: onSync');
	// },

	addContact: function(e) {

		console.log('App.Views.AddContact ::: initialize');
		e.preventDefault();

		console.log('App.Views.AddContact ::: attempt set first_name 	: ' + $('#first_name').val() );
		console.log('App.Views.AddContact ::: attempt set last_name 		: ' + $('#last_name').val() );
		console.log('App.Views.AddContact ::: attempt set email_address 	: ' + $('#email_address').val() );
		console.log('App.Views.AddContact ::: attempt set description 	: ' + $('#description').val() );

		var newContact = this.collection.create(
		{	
			first_name: this.$('#first_name').val(),
			last_name: this.$('#last_name').val(),
			email_address: this.$('#email_address').val(),
			description: this.$('#description').val()
		}, 
		{
			wait: true,
			success: function(){console.log('addcontact success')}, 
			error: function(){console.log('addcontact error')}
		});

		// this.collection.add(newContact);

		// newContact.fetch();

		console.log('App.Views.AddContact ::: addContact: ' + newContact);

		this.clearForm();
	},

	clearForm: function() {

		console.log('App.Views.AddContact ::: clearForm');

		this.first_name.val('');
		this.last_name.val('');
		this.email_address.val('');
		this.description.val('');
	}
});


/*
|---------------------------------------------
|
| Edit contact view
|
|---------------------------------------------
*/


App.Views.EditContact = Backbone.View.extend({

	template: template('editContactTemplate'),

	initialize: function() {
		this.render();
		this.form 			= this.$('form');
		this.first_name 	= this.form.find('#edit_first_name');
		this.last_name 		= this.form.find('#edit_last_name');
		this.email_address 	= this.form.find('#edit_email_address');
		this.description 	= this.form.find('#edit_description');
	},

	events: {
		'submit form': 'submit'
	},

	submit: function(e) {
		e.preventDefault();
		console.log( 'submit' );
		// grab model

		// update attributes
		// sync
		this.model.save({
			first_name: 	this.first_name.val();,
			last_name: 		this.last_name.val();,
			email_address: 	this.email_address.val();,
			description: 	this.description.val();
		});
	},

	render: function() {
		console.log('this.model.id 				= id: ' 			+ this.model.get('id'));
		console.log('this.model.first_name 		= first_name: ' 	+ this.model.get('first_name'));
		console.log('this.model.last_name 		= last_name: ' 		+ this.model.get('last_name'));
		console.log('this.model.email_address 	= email_address: ' 	+ this.model.get('email_address'));
		console.log('this.model.description 	= description: ' 	+ this.model.get('description'));
		
		this.$el.html( this.template( this.model.toJSON()) );
		return this;
	}
});



