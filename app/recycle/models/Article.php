<?php

class Article extends Eloquent {
	public $table = 'articles';
	public $timestamps = false;

	protected $fillable = array(
		'textblocks_text', 
		'textblocks_html_tags', 
		'textblocks_classes', 
		'vertical_padding_indexes', 
		'category'
	);

	public static function validate($data) 
	{
		$rules = array(
			'title' => 'required|min:1'
		);

		return Validator::make($data, $rules);
	}
}