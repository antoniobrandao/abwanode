<?php

class Task extends Eloquent {
	public $table = 'tasks';
	public $timestamps = false;

	protected $fillable = array( 'title', 'completed' );

	public static function validate($data) 
	{
		$rules = array(
			// 'title' => 'required|min:1'
		);

		return Validator::make($data, $rules);
	}
}