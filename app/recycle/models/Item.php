<?php

class Item extends Eloquent {
	public $table = 'items';
	public $timestamps = false;

	protected $fillable = array(
		'codename', 
		'category', 
		'title', 
		'subtitle', 
		'description', 
		'content_template', 
		'articles', 
		'medias', 
		'files', 
		'xtra1', 
		'xtra2'
	);

	public static function validate($data) 
	{
		$rules = array(
			'title' => 'required|min:1'
		);

		return Validator::make($data, $rules);
	}
}