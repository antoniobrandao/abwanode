<?php

class Contact extends Eloquent {
	public $table = 'contacts';
	public $timestamps = false;

	protected $fillable = array(
		'first_name', 
		'last_name', 
		'email_address', 
		'description'
	);

	// public static function validate($data) 
	// {
	// 	$rules = array(
	// 		'first_name' => 'required|min:1'
	// 	);

	// 	return Validator::make($data, $rules);
	// }
}

