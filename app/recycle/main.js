 (function(){

    window.App = {
        Models: {},
        Collections: {},
        Views: {},
        Router: {}
    };

    window.template = function(id) {
        return _.template( $('#' + id).html() );
    };

     // Person Model
    App.Models.Person = Backbone.Model.extend({
        defaults: {
            name: 'John Deo',
            age:30,
            occupation: 'slave'
        }
    }); 

        // validate: function(attrs) {
        //     if (attrs.age < 0) {
        //         return 'age must be positive';
        //     }

        //     if (!attrs.name) {
        //         return 'noname provided';
        //     }
        // },

        // work: function() {
        //     return this.get('name') + ' is working';
        // }




    // the View for a person
    App.Views.Person = Backbone.View.extend(
    {
        tagName: 'li',

        template: template('personTemplate'),

        // initialize: function () 
        // {
        //     // console.log(this.model);

        //     this.render();
        // },

        // className: 'testclass',
        // id: 'testid'

        render: function()
        {
            this.$el.html( this.template(this.model.toJSON()) );
            return this;
        }
    });

    App.Views.People = Backbone.View.extend({

        tagName: 'ul',

        initialize: function() {
            // this.render();

            // filter through all
            this.collection.each(function(person) 
            {
                // console.log(person); 
                
                var personView  = new App.Views.Person({ model: person });
                // console.log(personView.el);

                //attach to collectino root element
                // personView.render();
                this.$el.append(personView.render().el) ;

                // console.log(this);
                
            }, this)// context
            
        },

        render: function() {
            
            var num = 0;

            this.collection.each(function(person) 
            {
                // console.log(person); 
                
                // this.collection.models[num].render();

                num = num + 1;
                
            }, this)

            return this;
        }
    })

    // a list of People
    App.Collections.People = Backbone.Collection.extend({

        model: App.Models.Person

    });

    var person           = new App.Models.Person;
    var personView       = new App.Views.Person({ model: person });

    var person2          = new App.Models.Person({name: 'caralho'});
    var personView2      = new App.Views.Person({ model: person2 });


    var peopleCollection = new App.Collections.People( [person, person2] );
    peopleCollection.add( [person, person2] );

    // plural View
    var peopleView       = new App.Views.People( {collection: peopleCollection} )

    peopleView.render() 
    $(document.body).append(peopleView.el);

    // setTimeout(function(){
    //     person2.set('name', 'testNAME');
    //     peopleView.render()
    // }, 1000)


    //model = collection.at(index)



    ////
    ////
    ////



    App.Models.Task = Backbone.Model.extend({
        validate: function(attrs) {
            if (!attrs.title) {
                return 'title required';
            }

            if (attrs.title == '') {
                return 'title required';
            }
        }
    });

    App.Collections.Tasks = Backbone.Collection.extend({
        model: App.Models.Task
    });

    App.Views.Task = Backbone.View.extend({
        tagName: 'li',

        template: template('taskTemplate'),

        initialize: function() {
            // this.model.on('change:title')
            // _.bindAll(this, 'editTask', 'render');
            this.model.on('change', this.render, this) //this or bindall
            this.model.on('destroy', this.remove, this) //this or bindall
        },

        events: {
            'click .edit': 'editTask',
            'click .delete': 'destroyTask'
            // this.$el.find('.edit'); // slower
        },

        destroyTask: function() {
            this.model.destroy();
            console.log(tasksCollection);
        },

        remove: function() {
            this.$el.remove();
        },

        editTask: function() {
            // console.log(this.model.get('title'));
            var newTask = prompt('new value?', this.model.get('title'));

            newTask = $.trim(newTask);

            // if (!newTask) {
            //     return 'title required';
            // }

            // if (newTask == '') {
            //     return 'title required';
            // }

            // if (!newTask) return; // simple validation
            this.model.set('title', newTask, {validate:true});
        },
        // events: {
        //     'click span': 'showAlert'
        // },

        // showAlert: function() {
        //     console.log("sdkjfhkldsjf");
        // },

        render: function() {
            // console.log(this.template(this.model.toJSON()));
            var template = this.template( this.model.toJSON() );
            this.$el.html( template );
            // this.$el.html( this.model.get('title') );
            return this;
        }
    });

    var tasksCollection = new App.Collections.Tasks( // send array
        [
            {
                title: 'Go to the store',
                priority: 1
            },
            {
                title: 'Go to bed',
                priority: 2
            },
            {
                title: 'Go to hell',
                priority: 4
            }
        ]
    );

    // var taskView = new App.Views.Task({ model: task });

    // console.log(taskView.render().el);

    App.Views.Tasks = Backbone.View.extend({

        tagName: 'ul',

        initialize: function() {
            this.collection.on('add', this.addOne, this);
        },

        render: function() 
        {
            this.collection.each(this.addOne, this)
            return this;
        },

        addOne: function(task) 
        {    // responsibe for creating a child view and appending it to the root element
            var taskView = new App.Views.Task({ model:task });
            this.$el.append(taskView.render().el);
        }

    });

    App.Views.AddTask = Backbone.View.extend({

        el: '#addTask',

        initialize: function() {

        },

        events: {
            'submit': 'submit'
        },

        submit: function(e) {
            e.preventDefault();
            var newTaskTitle = $(e.currentTarget).find('input[type=text]').val()

            var task = new App.Models.Task({title: newTaskTitle });
            this.collection.add(task);
        }

    })

    // lalalala    = new App.Views.AddTask();
    tasksView   = new App.Views.Tasks({ collection: tasksCollection });

    addTaskView = new App.Views.AddTask({ collection: tasksCollection });

    $('.tasks').html(tasksView.render().el);

    var vent = _.extend({}, Backbone.Events);

    console.log(vent);

    App.Views.Appointments = Backbone.View.extend({
        initialize: function() {
            vent.on('appointment:show', this.show, this);
        },

        show: function(id) {
            console.log('showing appointment id = ' + id);
            
            var appointment     = this.collection.get(id)
            var appointmentView = new App.Views.appointment({ model: appointment })

            $(document.body).append(appointmentView.render().el)
        }
    });

    new App.Views.Appointments({ collection: someCollection });

    App.Router = Backbone.Router.extend({
        routes: {
            '': 'index',
            'show/:id': 'show',
            'download/:id/*filename': 'download',
            'search/:query': 'search',
            'appointment/:id': 'showAppointment',
            '*other': 'default'
        },

        index: function() {
            console.log('LALALAL');
        },

        showAppointment: function(appointmentId) {
            console.log('show appointment with id = ' + appointmentId);
            vent.trigger('appointment:show', appointmentId);
        },

        show: function(id) {
            console.log('show item with id = ' + id);
        },

        download: function(id, filename) {
            console.log(filename);
        },

        search: function(query) {
            console.log(query);
        },

        default: function(other) {
            console.log(other + ' not found 404'); // 404
        }
    })

    new App.Router;

    Backbone.history.start();
    
}());



    