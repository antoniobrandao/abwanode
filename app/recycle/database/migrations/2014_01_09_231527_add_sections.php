<?php

use Illuminate\Database\Migrations\Migration;

class AddSections extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('sections')->insert(array(
			'string_id'  	=> 'home',
			'page_group' 	=> '',
			'title' 		=> 'Home',
			'position' 		=> '0'
		));

		// DB::table('sections')->insert(array(
		// 	'stringid'  	=> 'about',
		// 	'rootpageid'	=> '',
		// 	'pagegroup' 	=> '',
		// 	'title' 		=> 'About Us',
		// 	'type' 			=> 'basic',
		// 	'position' 		=> '1'
		// ));

		// DB::table('sections')->insert(array(
		// 	'stringid'  	=> 'contact',
		// 	'rootpageid'	=> '',
		// 	'pagegroup' 	=> '',
		// 	'title' 		=> 'Contact',
		// 	'type' 			=> 'basic',
		// 	'position' 		=> '2'
		// ));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Section::truncate();
	}
}