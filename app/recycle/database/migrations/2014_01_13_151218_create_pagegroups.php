<?php

use Illuminate\Database\Migrations\Migration;

class CreatePagegroups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pagegroups', function($table){
			
			$table->increments('id');
			
			$table->string('title');
			$table->string('subtitle');
			$table->text('description');
			
			$table->text('elements');			// Array in string format of the ids of contained PAGES
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pagegroups');
	}

}