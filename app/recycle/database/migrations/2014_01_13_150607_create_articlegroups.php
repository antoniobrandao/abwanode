<?php

use Illuminate\Database\Migrations\Migration;

class CreateArticlegroups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articlegroups', function($table){
			
			$table->increments('id');
			
			$table->string('string_id');		// to be used in friendly urls (with jquery address)
			$table->string('codename'); 	// group codename PLURAL: Like "Posts", "Notes", "Reports", "Essays"..

			$table->string('title');
			$table->string('subtitle');
			$table->text('description');

			$table->text('elements');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articlegroups');
	}

}