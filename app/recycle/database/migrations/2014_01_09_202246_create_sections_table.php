<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sections', function(Blueprint $table){
			
			$table->increments('id');
			
			// editable
			$table->string('title');
			$table->string('string_id');
			$table->string('image');
			
			// system only
			$table->string('parent_section_string_id');
			$table->string('page_group'); // contains 1 group of pages
			$table->string('position');

			// $table->string('type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sections');
	}

}