<?php

// class TasksTableSeeder extends Seeder {

//     public function run()
//     {
//         $this->call('TasksTableSeederFunction');

//         $this->command->info('Tasks table seeded!');
//     }

// }

class TasksTableSeeder extends Seeder {

    public function run()
    {
        DB::table('tasks')->truncate();

        $tasks = [
         [ 
             'title' => 'Home task title'
         ],

         [ 
             'title' => 'About task title'
         ],

         [ 
             'title' => 'Contact task title'
         ],

        ];

        DB::table('tasks')->insert($tasks);
    }

}