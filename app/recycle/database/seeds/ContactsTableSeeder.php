<?php

// class TasksTableSeeder extends Seeder {

//     public function run()
//     {
//         $this->call('TasksTableSeederFunction');

//         $this->command->info('Tasks table seeded!');
//     }

// }

class ContactsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('contacts')->truncate();

        $contacts = [
         [ 
             'first_name' => 'John',
             'last_name' => 'White',
             'email_address' => 'koikoi@koi.koi',
             'description' => 'desciption 1'
         ],
         [ 
             'first_name' => 'Sherlock',
             'last_name' => 'Holmes',
             'email_address' => 'sdfg@.vo',
             'description' => 'desciption 2'
         ],
         [ 
             'first_name' => 'Che',
             'last_name' => ' Guevara',
             'email_address' => 'sfdf@esfr.de',
             'description' => 'desciption 3'
         ]

        ];

        DB::table('contacts')->insert($contacts);
    }

}