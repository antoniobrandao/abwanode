<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::resource('contacts', 'ContactsController');





Route::resource('sections', 		'SectionsController');

Route::resource('sitesettings', 'SitesettingsController');


// Route::resource('pages', 			'PagesController');

// Route::resource('itemgroups', 		'ItemGroupsController');
// Route::resource('imagegroups', 		'ImageGroupsController');
// Route::resource('articlegroups', 	'ArticleGroupsController');
// Route::resource('filegroups', 		'FileGroupsController');

// Route::get('/', function()
// {
	// return View::make('home');
// 	return View::make('dashboardcontroller@index');
// });

Route::get('/',
array(
		'before' 	=> 'auth',
		'as'		=>'dashboard', 
		'uses'		=>'dashboardcontroller@index'
));

// Route::post('contacts', function()
// {
//     'as' => 'contacts_post_route',
// 	'uses'=>'contactscontroller@store'	
// });

// Route::post( 'contacts', array(
//     'as' => 'contacts_post_route',
// 	'uses'=>'contactscontroller@store'
// ));

// Route::get('/', function()
// {
// 	return View::make('frontend.hello');
// });

// Route::get( 'tasks', array(
//     'as' => 'tasks_route',
// 	'uses'=>'taskscontroller@index'
// ));

// Route::post( 'tasks', array(
//     'as' => 'tasks_post_route',
// 	'uses'=>'taskscontroller@store'
// ));

// Route::get( 'tasks/{anyaddress}', array(
//     'as' => 'task_route',
// 	'uses'=>'taskscontroller@show'
// ));

// Route::put( 'tasks/{anyaddress}', array(
//     'as' => 'task_put_route',
// 	'uses'=>'taskscontroller@update'
// ));

// Route::post( 'tasks/{anyaddress}', array(
//     'as' => 'task_post_route',
// 	'uses'=>'taskscontroller@store'
// ));

// Route::delete( 'tasks/{anyaddress}', array(
//     'as' => 'task_delete_route',
// 	'uses'=>'taskscontroller@destroy'
// ));

// Route::controller('tasks', 'TasksController');
// Route::resource('tasks', 'TasksController');

// Route::post( 'updatesitetitle', array(
//     'as' => 'update_site_title',
//     'uses' => 'dashboardcontroller@updatesitetitle'
// ));

/// 

//Settings: update site title
// Route::post( 'updatesitetitle', array(
//     'as' => 'update_site_title',
//     'uses' => 'dashboardcontroller@updatesitetitle'
// ));

// Route::get('admin', array(
// 	'before' => 'auth',
// 	'as'=>'dashboardcontroller', 
// 	'uses'=>'dashboardcontroller@index'
// ));

// Route::post('image_store', array(
// 	'as'=>'image_store_route', 
// 	'uses'=>'dashboardcontroller@store'
// ));

// Route::get('sections', array(
// 	'before' => 'auth',
// 	'as'=>'sections', 
// 	'uses'=>'sections@index'
// ));

// Route::get('section/{anyaddress}', array(
// 	'before' => 'auth',
// 	'as' => 'section', 
// 	'uses'=> 'sections@view'
// ));	

// Route::get('sections/new', array(
// 	'before' => 'auth',
// 	'as'=>'new_section',
// 	'uses'=>'sections@newsection'
// ));

// Route::post('sections/create', array(
// 	'before'=>'csrf',
// 	'before' => 'auth',
// 	'as'=>'create_section', 
// 	'uses'=>'sections@createsection'
// ));

// Route::delete('sections/delete', array(
// 	'before'=>'csrf',
// 	'before' => 'auth',
// 	'as'=>'delete_section', 
// 	'uses'=>'sections@deletesection'
// ));

// Route::put('sections/update', array(
// 	'before'=>'csrf',
// 	'before' => 'auth',
// 	'as'=>'update_section',
// 	'uses'=>'sections@updatesection'
// ));

// Route::get('sections/{anyaddress}/edit', array(
// 	'before' => 'auth',
// 	'as'=>'edit_section',
// 	'uses'=>'sections@editsection'
// ));


// AUTH

Route::get('login', function()
{
	return View::make('auth.login');
});

Route::get('signup', function()
{
	return View::make('auth.signup');
});


Route::post('login', function()
{
	$userdata = array(
		'email' => Input::get('email'),
		'password' => Input::get('password'),
	);

	if (Auth::attempt($userdata)) 
	{
		echo 'successful';
		return Redirect::to('');
	} 
	else 
	{
		echo "not successful";
		return View::make('auth.login');
	}
});

Route::post('signup', function()
{
	$validated = true;
	Log::info('LOG 1');

	if ($validated) 
	{
		Log::info('LOG 2');
		$userdata = array(
			'username' => Input::get('username'),
			'email' => Input::get('email'),
			'password' => Hash::make(Input::get('password'))
		);	

		$user = new User($userdata);
		$user->save();
		Log::info('LOG 3');
		return Redirect::to('login');
	}
	else
	{
		Log::info('LOG 4');
		echo "bad data";
		return View::make('auth.signup');
	}
});



Route::get('logout', function()
{
	Auth::logout();
	return Redirect::to('login');
});

