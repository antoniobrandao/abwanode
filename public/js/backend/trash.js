


////////////        ######## ########     ###     ######  ##     ## 
////////////           ##    ##     ##   ## ##   ##    ## ##     ## 
////////////           ##    ##     ##  ##   ##  ##       ##     ## 
////////////           ##    ########  ##     ##  ######  ######### 
////////////           ##    ##   ##   #########       ## ##     ## 
////////////           ##    ##    ##  ##     ## ##    ## ##     ## 
////////////           ##    ##     ## ##     ##  ######  ##     ## 


    /*
    ---------------------------------------------
    
    app init
    
    ---------------------------------------------
    */

    window.App = 
    {
        Models: {},
        Collections: {},
        Views: {},
        Router: {}
    };

    /*
    ---------------------------------------------
    
    event manager
    
    ---------------------------------------------
    */
    
    // var vent = _.extend({}, Backbone.Events);

    /*
    ---------------------------------------------
    
    ajax error logger
    
    ---------------------------------------------
    */
    
    $(document).ajaxError(function (e, xhr, options) 
    {    
        console.log('- AJAX log message -');
        // console.log('e: ' + e);
        console.log('xhr: ' + xhr.responseText);
        // console.log('options: ' + options.responseText);
    });

    /*
    ---------------------------------------------
    
    helpers
    
    ---------------------------------------------
    */

    window.template = function(id) {
        return _.template( $('#' + id).html() );
    };

    // _.templateSettings = {
    //     interpolate : /\{\{([\s\S]+?)\}\}/g
    // };
    
    // window.assign = function (view, selector) {
    //     view.setElement(this.$(selector)).render();
    // };

    // to use assign:
    // render : function () {
    //     this.$el.html(this.template());

    //     this.assign(this.subview,        '.subview');
    //     this.assign(this.anotherSubview, '.another-subview');
    //     return this;
    // }















// on a  model:


    // attributesChanged: function(){
    //  console.log('App.Models.Section ::: attributesChanged');
        // var valid = false;
        // if (this.get('username') && this.get('password'))
        //   valid = true;
        // this.trigger("validated", valid);
    // },

    // validate: function(attrs) {
    //  if ( !attrs.title ) {
    //      return "email_address are required";
    //  };
    // },


// on a model: end


// boilerplate
// App.Views.View = Backbone.View.extend({

    // tagName: 'li',

    // // template: template('allContactsTemplate'),

    // initialize: function(){
    //     console.log('App.Views.Section ::: initialize');

    //     // _.bindAll(this, this.render);
    //     // this.model.bind('change', this.render);


    //     // this.model.on('destroy', this.unrender, this);

    //     // this.model.on('sync', this.onAAyncEvent, this);
    //     // this.model.on('reset', this.onTestEvent, this);
    // },

    // events: {
    //  'click a.delete': 'deleteContact',
    //  // 'click a.edit': 'editContact'
    // },

    // // testEvent: function() {
    // //  console.log("0988098098098");
    // // },

    // deleteContact: function() {

    //  console.log('App.Views.Section ::: deleteContact');

    //  // this.model.destroy();
    // },

    // editSection: function() {
        // vent.trigger('contact:edit', this.model);
        // console.log('App.Views.Section ::: editContact');
        // console.log('App.Views.Section ::: editContact - this.model: ' + this.model);

        // this.model.fetch( {success : console.log('333 33 3 3 3 33 3 this.model: '+ this.model) });

        // create new EditContactView
        // bind the model
        // append to the DOM
    // },

    // onAAyncEvent: function(model) {
    //  console.log('SYNC: detected sync');

        // var editContactView = new App.Views.EditContact({ model: this.model });
    // },

    // render: function() {

    //     console.log('App.Views.Section ::: render');

    //     // this.$el.html( this.template( this.model.toJSON() ) );
        
    //     this.$el.html( this.model.get('title') );

    //     return this;
    // }

    // unrender: function() {

    //  console.log('App.Views.Section ::: unrender');

    //  // this.remove();  // this.$el.remove
    // }
// });











// App.Views.Sitesettings = Backbone.View.extend({
    
//     tagName: 'ul',

//     initialize: function() {

//         console.log('App.Views.Sections ::: initialize');

        // _.bindAll(this, this.render);

        // this.model.bind('change', this.render);

        // this.collection.on('change',  this.onChangeEvent );

        // this.collection.on('add', this.addOne, this);

        // this.collection.on('reset', this.onResetEvent, this);
        // this.collection.on('sync', this.onAAyncEvent, this);
        
        // this.collection.on('reset', function(model) {
        //  console.log('RESET: detected fetch');
        // });

        // this.collection.on('sync', function(model) {
        //  console.log('SYNC: detected fetch');
        // });
    // },
    
    // onChangeEvent: function(model) {

        // console.log('App.Views.Sections ::: onChangeEvent');
        // App.contacts.fetch();
    // },

    // onResetEvent: function(model) {

    //  console.log('App.Views.Sections ::: onResetEvent');
    // },

    // onAAyncEvent: function(model) {
    //  console.log('SYNC: detected sync');
    // },

//     render: function() {

//         this.$el.empty();

//         this.collection.each( this.addOne, this );

//         console.log('App.Views.Sections ::: render ' + 'this.el: ' + this.el);
        
//         return this;
//     },

//     addOne: function(contactModel) {

//      console.log('App.Views.Sections ::: addOne');

//      var new_child_view = new App.Views.Section({ model: contactModel });
        
//      this.$el.append( new_child_view.render().el );
//     }

// });




    /// /// APP SPECIFIC /// ///
    /// /// APP SPECIFIC /// ///
    /// /// APP SPECIFIC /// ///


    // App.Models.Task = Backbone.Model.extend({
    //     defaults: {
    //         title: 'lalala',
    //         completed: 0
    //     }
    // })

    // App.Collections.Tasks = Backbone.Collection.extend({
    //     model: App.Models.Task,
    //     url: '/tasks'
    // });

    // App.Views.Tasks = Backbone.View.extend({
    //     tagName: 'ul',

    //     initialize: function() {
    //         this.collection.on('add', this.addOne, this);
    //     },

    //     render: function() {
    //         this.$el.empty();
    //         this.collection.each(this.addOne, this);
    //         return this;
    //     },

    //     addOne: function(task) {
    //         var task = new App.Views.Task({ model: task });
    //         this.$el.append( task.render().el );
    //     }
    // });

    // App.Views.Task = Backbone.View.extend({
    //     tagName: 'li',

    //     initialize: function() {
    //         this.model.on('destroy', this.remove, this)
    //     },

    //     render: function( ) {
    //         this.$el.html( this.model.get('title') );
    //         return this;
    //     }
    // });