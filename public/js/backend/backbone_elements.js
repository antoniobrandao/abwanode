 (function(){




////////////           ###    ########  ########      ######   #######  ########  ######## 
////////////          ## ##   ##     ## ##     ##    ##    ## ##     ## ##     ## ##       
////////////         ##   ##  ##     ## ##     ##    ##       ##     ## ##     ## ##       
////////////        ##     ## ########  ########     ##       ##     ## ########  ######   
////////////        ######### ##        ##           ##       ##     ## ##   ##   ##       
////////////        ##     ## ##        ##           ##    ## ##     ## ##    ##  ##       
////////////        ##     ## ##        ##            ######   #######  ##     ## ######## 


window.App = 
{
    Models: {},
    Collections: {},
    Views: {},
    Templates: {},
    Router: {},
};

// events
var vent = _.extend({}, Backbone.Events);


// ajax error logger
$(document).ajaxError(function (e, xhr, options) 
{    
    console.log('- AJAX log message -');
    console.log('e: ' + e);
    console.log('xhr: ' + xhr.responseText);
    console.log('options: ' + options.responseText);
});

// init template factory
window.require_template = function(templateName) 
{
    var template = $('#' + templateName);
    if (template.length === 0) {
        var tmpl_dir = './templates';
        var tmpl_url = tmpl_dir + '/' + templateName + '.tmpl';
        var tmpl_string = '';

        $.ajax({
            url: tmpl_url,
            method: 'GET',
            async: false,
            contentType: 'text',
            success: function (data) {
                tmpl_string = data;
                $('head').append('<script id="' + templateName + '" type="text/x-handlebars-template">' + tmpl_string + '<\/script>');
            }
        });
    }
}

require_template('template_sitesetting');




////////////        ##     ##  #######  ########  ######## ##        ######  
////////////        ###   ### ##     ## ##     ## ##       ##       ##    ## 
////////////        #### #### ##     ## ##     ## ##       ##       ##       
////////////        ## ### ## ##     ## ##     ## ######   ##        ######  
////////////        ##     ## ##     ## ##     ## ##       ##             ## 
////////////        ##     ## ##     ## ##     ## ##       ##       ##    ## 
////////////        ##     ##  #######  ########  ######## ########  ######  


App.Models.Section = Backbone.Model.extend({
    
    // idAttribute  : "id",

    urlRoot: 'sections',

    defaults: {
        'title':'An Elegant Title'
    }
}); 


App.Models.Sitesetting = Backbone.Model.extend({
    
    // idAttribute  : "id",

    urlRoot: 'sitesettings',

    defaults: {
        'title':'An Elegant Title'
    }
}); 


App.Models.Media = Backbone.Model.extend({
    urlRoot: 'medias'
}); 


////////////         ######   #######  ##       ##       ########  ######  ######## ####  #######  ##    ##  ######  
////////////        ##    ## ##     ## ##       ##       ##       ##    ##    ##     ##  ##     ## ###   ## ##    ## 
////////////        ##       ##     ## ##       ##       ##       ##          ##     ##  ##     ## ####  ## ##       
////////////        ##       ##     ## ##       ##       ######   ##          ##     ##  ##     ## ## ## ##  ######  
////////////        ##       ##     ## ##       ##       ##       ##          ##     ##  ##     ## ##  ####       ## 
////////////        ##    ## ##     ## ##       ##       ##       ##    ##    ##     ##  ##     ## ##   ### ##    ## 
////////////         ######   #######  ######## ######## ########  ######     ##    ####  #######  ##    ##  ######  


App.Collections.Sections = Backbone.Collection.extend(
{
    model: App.Models.Section,
    url: '/sections'
});
    

App.Collections.Sitesettings = Backbone.Collection.extend(
{
    model: App.Models.Sitesetting,
    url: '/sitesettings'
});


App.Collections.Medias = Backbone.Collection.extend(
{
    model: App.Models.Media,
    url: '/medias'
});


////////////        ##     ## #### ######## ##      ##  ######  
////////////        ##     ##  ##  ##       ##  ##  ## ##    ## 
////////////        ##     ##  ##  ##       ##  ##  ## ##       
////////////        ##     ##  ##  ######   ##  ##  ##  ######  
////////////         ##   ##   ##  ##       ##  ##  ##       ## 
////////////          ## ##    ##  ##       ##  ##  ## ##    ## 
////////////           ###    #### ########  ###  ###   ######  


App.Views.Section = Backbone.View.extend({

    tagName: 'li',

    initialize: function(){
        console.log('App.Views.Section ::: initialize');
        
        this.collection.on('sync', function(model) {
         console.log('SYNC: detected fetch');
        });
    },

    render: function() {

        console.log('App.Views.Section ::: render');
        
        this.$el.html( this.model.get('title') );

        return this;
    }
});


App.Views.Sitesetting = Backbone.View.extend(
{
    tagName: 'div',

    initialize: function() {
        console.log('App.Views.Sitesetting ::: initialize');
        console.log(this.updateImage);

        // $('#settings_form > #image').bind('change', this.updateImage);
        // $('#image').change('change', function() {
        //     console.log('THEERE');
        // });

        // $('#image').onchange = function () {
        //     console.log('THEERE');
        //     alert(this.value);
        // }

        // _.bindAll(this, this.render);
    },

    events: {
        'submit': 'updateImage'
    },

    updateImage: function(e) {

        console.log('App.Views.Sitesetting ::: updateSitesettings');

        e.preventDefault();

        var settings_model      = App.sitesettings.get(1);
        var related_media_model = App.medias.get(App.sitesettings.get(1).get('image_id'));

        console.log('there !');
        console.log("FILE path: " + this.$('#image').val());

        var formData = new FormData($('form')[0]);
        
        // alert('uploadimage/' + App.sitesettings.get(1).get('image_id'));

        test = $.ajax({
            url: 'uploadimage/' + App.sitesettings.get(1).get('image_id'),  //Server script to process data
            type: 'POST',
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){ // Check if upload property exists
                    myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    console.log('pload property exists !!');
                }
                return myXhr;
            },
            //Ajax events
            beforeSend: beforeSendHandler,
            success: completeHandler,
            error: errorHandler,
            // Form data
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false
        });

        function beforeSendHandler(e){
            // console.log('beforeSendHandler');
        }

        function completeHandler(e){
            // console.log('completeHandler');
            // console.log('completeHandler: ' + related_media_model.get('url'));

            related_media_model.fetch().then(function()
            {
                var oldSrc = $('#image_holder').attr('src');
                var newSrc = App.medias.get(App.sitesettings.get(1).get('image_id')).get('url');

                console.log('oldSrc: '+ oldSrc);
                console.log('newSrc: '+ newSrc);

                $('img[src="' + oldSrc + '"]').attr('src', newSrc);
            })
        }

        function errorHandler(e){
            console.log('errorHandler');
        }

        function progressHandlingFunction(e){
            console.log('upload progress');
            // if(e.lengthComputable){
            //     $('progress').attr({value:e.loaded,max:e.total});
            // }
        }
    },

    render: function() {

        console.log('App.Views.Sitesetting ::: render');

        this.template = Handlebars.compile($('#template_sitesetting').html());

        var media = App.medias.get(this.model.get('image_id'));

        var context = {
            title:          this.model.get('title'), 
            subtitle:       this.model.get('subtitle'), 
            description:    this.model.get('description'),
            image_url:      media.get('url')
        };

        var html    = this.template(context);

        this.el = html;
        this.$el.html( html );
        return this;
    }
});


////////////        ##     ## #### ######## ##      ##        ##     ## ##     ## ##       ######## #### 
////////////        ##     ##  ##  ##       ##  ##  ##        ###   ### ##     ## ##          ##     ##  
////////////        ##     ##  ##  ##       ##  ##  ##        #### #### ##     ## ##          ##     ##  
////////////        ##     ##  ##  ######   ##  ##  ##        ## ### ## ##     ## ##          ##     ##  
////////////         ##   ##   ##  ##       ##  ##  ##        ##     ## ##     ## ##          ##     ##  
////////////          ## ##    ##  ##       ##  ##  ##        ##     ## ##     ## ##          ##     ##  
////////////           ###    #### ########  ###  ###         ##     ##  #######  ########    ##    #### 


App.Views.Sections = Backbone.View.extend({
    
    tagName: 'ul',

    initialize: function() {

        console.log('App.Views.Sections ::: initialize');

        // _.bindAll(this, this.render);    
    },

    render: function() {

        this.$el.empty();

        this.collection.each( this.addOne, this );

        console.log('App.Views.Sections ::: render ' + 'this.el: ' + this.el);
        
        return this;
    },

    addOne: function(model) {

     console.log('App.Views.Sections ::: addOne');

     // var new_child_view = new App.Views.Section({ model: model });
        
     // this.$el.append( new_child_view.render().el );
    }

});


App.Views.Sitesettings = Backbone.View.extend({
    
    tagName: 'ul',

    initialize: function() {

        console.log('App.Views.Sitesettings ::: initialize');
        
        // _.bindAll(this, this.render);
    },

    render: function() {


        this.$el.empty();

        this.collection.each( this.addOne, this );

        console.log('App.Views.Sitesettings ::: render ' + 'this.el: ' + this.el);
        
        return this;
    },

    addOne: function(model) {

        console.log('App.Views.Sitesettings ::: addOne');

        var new_child_view = new App.Views.Sitesetting({ model: model });
        
        this.$el.append( new_child_view.render().el );
    }

});







////////////        ########   #######  ##     ## ######## ######## ########  
////////////        ##     ## ##     ## ##     ##    ##    ##       ##     ## 
////////////        ##     ## ##     ## ##     ##    ##    ##       ##     ## 
////////////        ########  ##     ## ##     ##    ##    ######   ########  
////////////        ##   ##   ##     ## ##     ##    ##    ##       ##   ##   
////////////        ##    ##  ##     ## ##     ##    ##    ##       ##    ##  
////////////        ##     ##  #######   #######     ##    ######## ##     ## 


// App.Router = Backbone.Router.extend({
//     routes: {
//         '': 'index'
//     },

//     index: function() {
//         console.log('index route reached');
//     }

    // showAppointment: function(appointmentId) {
    //     console.log('show appointment with id = ' + appointmentId);
    //     vent.trigger('appointment:show', appointmentId);
    // },

    // show: function(id) {
    //     console.log('show item with id = ' + id);
    // },

    // download: function(id, filename) {
    //     console.log(filename);
    // },

    // search: function(query) {
    //     console.log(query);
    // },

    // default: function(other) {
    //     console.log(other + ' not found 404'); // 404
    // }
// });

// new App.Router;

// Backbone.history.start();
















// App.Views.MediaImageView = Backbone.View.extend({

//     tagName: 'div',

//     initialize: function(){
//         console.log('App.Views.MediaImageView ::: initialize');
//     },

//     render: function() {

//         console.log('App.Views.MediaImageView ::: render');

//         this.$el.html( '<img src="' + this.model.get('url') + '">' );

//         return this;
//     }
// });

}());