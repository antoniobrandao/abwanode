var Person = Backbone.Model.extend({
	defaults: {
		name: 'John Deo',
		age:30,
		occupation: 'slave'
	},

	validate: function() {

	},

	work: function() {
		return this.get('name') + ' is working';
	}
})