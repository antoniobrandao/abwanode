## AB WMS

ABWMS is an simple, elegant website creation and management web tool.

ABWMS aims to drastically simplify the process of creating a regular website, by providing the means to setup the sections, pages, content types, and fundamental content elements. With ABCMS, creating specific database tables for a website is a thing of the past.

Each ABWMS instance generates a ABCMS, a sibling CMS web tool specific to manage content. ABCMS is a tool for the end-user to manage the website's content. 

## Official Documentation

Documentation for the entire tool can be found on [Antonio Brandao's brain](mailto:antoniobrandaodesign@gmail.com).

### Contributing To ABWMS

Talk to [Antonio Brandao](http://www.antoniobrandao.com).

### License

Private stuff
